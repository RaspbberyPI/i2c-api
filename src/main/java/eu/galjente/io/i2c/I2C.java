package eu.galjente.io.i2c;

import java.io.IOException;

/**
 * This interface representing API of simple I2C usage, just basic operations:
 * Read, Write, Read from register, Write to register.
 */
public interface I2C extends AutoCloseable {

    /**
     * Read data from I2C device
     * @return byte value
     * @throws IOException in case if connection closed or failed to read data
     */
    byte read() throws IOException;

    /**
     * Read register value from I2C device
     * @param register address of register
     * @return register value
     * @throws IOException in case if connection closed or failed to read data
     */
    byte readRegister(byte register) throws IOException;

    /**
     * Read register word(2 byte) value from I2C device
     * @param register address of register
     * @return register value
     * @throws IOException in case if connection closed or failed to read data
     */
    short readWordRegister(byte register) throws IOException;

    /**
     * Write date to I2C device
     * @param value byte value
     * @throws IOException in case if connection closed or failed to write data
     */
    void write(byte value) throws IOException;

    /**
     * Write register data to I2C device
     * @param register address of register
     * @param value byte value for register
     * @throws IOException in case if connection closed or failed to write data
     */
    void writeRegister(byte register, byte value) throws IOException;

    /**
     * Write register word(2 byte) data to I2C device
     * @param register address of register
     * @param value short(word) value for register
     * @throws IOException in case if connection closed or failed to write data
     */
    void writeRegister(byte register, short value) throws IOException;

}

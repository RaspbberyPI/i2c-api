package eu.galjente.io.i2c;

import java.io.IOException;

/**
 * This factory interface representing creating of I2C device client
 */
public interface I2CFactory {

    /**
     * Create I2C device client
     * @param address device address
     * @return I2C client
     * @throws IOException when failed connect to I2C device by specified address
     */
    I2C create(byte address) throws IOException;

}
